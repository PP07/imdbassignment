﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.Domain
{
    public class Movie
    {
        public string Name { get; set; }

        public string Plot { get; set; }

        public int YOR { get; set; }

        public List<Actor> ActorList = new List<Actor>();

        public List<Producer> ProducerList = new List<Producer>();
        public Movie(string name, string plot,int yearReal,List<Actor> actorlist,List<Producer> producerlist)
        {

            Name = name;
            Plot = plot;
            YOR = yearReal;
            ActorList = actorlist;
            ProducerList = producerlist;
        }
        public Movie()
        {
        }
    }
}
