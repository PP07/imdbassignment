﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.Domain
{
    public class Actor 
    {
        public string Name { get; set; }
        
        public DateTime Born { get; set; }

        public Actor(string name,DateTime born)
        {

            Name = name;

            Born = born;
        }

        public Actor()
        {
        }
    }
}
