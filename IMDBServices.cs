﻿using System;
using System.Collections.Generic;

using IMDB.Domain;
using IMDB;
using System.Linq;

namespace IMDB_DEMO
{
    public class IMDBServices
    {
        private readonly ActorRepository _actorrepository;
        private readonly MovieRepository _movierepository;
        private readonly ProducerRepository _producerrepository;

        public IMDBServices()
        {
            _actorrepository = new ActorRepository();
            _movierepository = new MovieRepository();
            _producerrepository = new ProducerRepository();
        }
        public void AddActor(string name,DateTime born)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Invalid arguments");
            }

            Actor actor = new Actor()
            {
                Name = name,
                Born = born,
            };

            _actorrepository.Add(actor);
        }
        public void AddProducer(string name,DateTime born)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Invalid arguments");
            }

            Producer producer = new Producer()
            {
                Name = name,
                Born = born,
            };
            _producerrepository.Add(producer);
        }
        public void AddMovie(string name,string plot,int yearRelease,List<int> actorlist,List<int> producerlist)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Invalid arguments");
            }
            List<Producer> producerList = new List<Producer>();
            foreach (var producer in producerlist)
            {
                producerList.Add(_producerrepository.Get()[producer-1]);
            }
            List<Actor> Actorlist = new List<Actor>();
            foreach (var actor in actorlist)
            {
                Actorlist.Add(_actorrepository.Get()[actor-1]);
            }
                Movie movie = new Movie()
            {
                Name = name,
                Plot = plot,
                YOR = yearRelease,
                ActorList =  Actorlist,
                ProducerList = producerList,
            };
            _movierepository.Add(movie);
        }
        public List<Movie> GetMovies()
        {
            return _movierepository.Get();
        }
        public List<Actor> GetActors()
        {
            return _actorrepository.Get();
        }
        public List<Producer> GetProducers()
        {
            return _producerrepository.Get();
        }
        public void DeleteMovie(string movieToDelete)
        {
            var movies = GetMovies();
            var deleted = movies.First((X => X.Name == movieToDelete));
            _movierepository.Delete(deleted);
        }
    }
}
