﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.Domain
{
    public class Producer
    {
        public string Name { get; set; }

        public DateTime Born ;
        public Producer(string name,DateTime born)
        {

            Name = name;
            Born = born;
        }

        public Producer()
        {
        }
    }
}
