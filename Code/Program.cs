﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMDB.Domain;

namespace IMDB_DEMO
{
    class Program
    {
        static void Main(string[] args)
        {
            IMDBServices  im = new IMDBServices();
            im.AddActor("Nick Jonas", DateTime.ParseExact("24/01/2013", "dd/MM/yyyy", CultureInfo.InvariantCulture));
            im.AddActor("Adam", DateTime.ParseExact("23/07/2003", "dd/MM/yyyy", CultureInfo.InvariantCulture));
            im.AddActor("Daniel Sams", DateTime.ParseExact("04/01/2011", "dd/MM/yyyy", CultureInfo.InvariantCulture));

            im.AddProducer("Steve", DateTime.ParseExact("20/01/2000", "dd/MM/yyyy", CultureInfo.InvariantCulture));
            im.AddProducer("Marcus", DateTime.ParseExact("10/01/2005", "dd/MM/yyyy", CultureInfo.InvariantCulture));
            im.AddProducer("Paul", DateTime.ParseExact("12/05/2007", "dd/MM/yyyy", CultureInfo.InvariantCulture));

            im.AddMovie("Deadpool 1", "Deadpool came to save", 2009, new List<int> { 1, 2 }, new List<int> { 1 });
            im.AddMovie("Deadpool 2", "Deadpool Supported people ", 2003, new List<int> { 3, 2 }, new List<int> { 2 });
            im.AddMovie("Deadpool 3", "Deadpool upgraded his power", 2010, new List<int> { 2, 3 }, new List<int> { 3 });
            im.AddMovie("Avengers : The EndGame", "Captain America", 2018, new List<int> { 3, 2 }, new List<int> { 2 });
            im.AddMovie("Sherlock Holmes", "Mystery and Search continues", 2000, new List<int> { 3, 1 }, new List<int> { 1 });
            im.AddMovie("One Man Army", "Fight and Adventure",2015, new List<int> { 1 }, new List<int> { 2 });
            Console.WriteLine("Menu Driven Program:");
            while (true)
            {
                Console.WriteLine("1.List Movies");
                Console.WriteLine("2.Add Movie");
                Console.WriteLine("3.Add Actor");
                Console.WriteLine("4.Add Producer");
                Console.WriteLine("5.Delete Movie");
                Console.WriteLine("6.Press key 6 to exit");
                Console.WriteLine("Enter your choice:");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Program to List Movie");
                        var AllMovieList = im.GetMovies();
                        foreach(var movie in AllMovieList)
                        {
                            Console.WriteLine("Name:"+movie.Name);
                            Console.WriteLine("Year of Release:"+movie.YOR);
                            Console.WriteLine("Plot:"+movie.Plot);
                            foreach(var ac in movie.ActorList)
                            {
                                Console.WriteLine(ac.Name);
                            }
                            foreach(var pd in movie.ProducerList)
                            {
                                Console.WriteLine(pd.Name);
                            }
                        }
                        Console.WriteLine("MOVIES");
                        break;
                    case 2:
                        Console.WriteLine("Program to ADD Movie");
                        Console.Write("Name : ");
                        string name = Console.ReadLine();
                        Console.Write("Plot : ");
                        string plot = Console.ReadLine();
                        Console.Write("Year of release : ");
                        int year = Convert.ToInt32( Console.ReadLine());
                        var actors = im.GetActors();
                        Console.Write("Choose Actors: ");
                        for (int i = 0; i < actors.Count; i++)
                        {
                            Console.Write($"{i+1}. {actors[i].Name} \t");
                        }
                        Console.WriteLine();
                        string actorInd = Console.ReadLine();
                        List<int> actorIndices = new List<int>();
                        foreach(var i in actorInd.Split(' '))
                        {
                            actorIndices.Add(Convert.ToInt32(i));
                        }
                        Console.WriteLine();
                        Console.WriteLine("Choose Producers: ");
                        var producers = im.GetProducers();
                        for (int i = 0; i<producers.Count; i++)
                        {
                            Console.Write($"{i + 1}. {producers[i].Name} \t");
                        }
                        Console.WriteLine();
                        string producerImd = Console.ReadLine();
                        List<int> producerIndice= new List<int>();
                        producerIndice.Add(Convert.ToInt32(producerImd) - 1);
                        im.AddMovie(name, plot, year, actorIndices, producerIndice );
                        Console.WriteLine("\n");
                        break;
                    case 3:
                        Console.WriteLine("Program to ADD Actor");
                        Console.WriteLine("Name:");
                        string actorName = Console.ReadLine();
                        Console.WriteLine("DOB in MM/dd/yyyy");
                        DateTime dobActor;
                        if (DateTime.TryParseExact(Console.ReadLine(), "MM/dd/yyyy",
                                                  CultureInfo.InvariantCulture,
                                                  DateTimeStyles.None, out dobActor))
                        {
                            // Your input string can (and will) be parsed with MM/dd/yyyy format.
                        }
                        else
                        {
                            // Invalid format or value.
                        }
                        im.AddActor(actorName, dobActor);
                        Console.WriteLine("\n");
                        break;
                    case 4:
                        Console.WriteLine("Program to ADD Producer");
                        Console.WriteLine("Name:");
                        string producerName = Console.ReadLine();
                        Console.WriteLine("DOB in MM/dd/yyyy");
                        DateTime dobProducer;
                        if (DateTime.TryParseExact(Console.ReadLine(), "MM/dd/yyyy",
                                                  CultureInfo.InvariantCulture,
                                                  DateTimeStyles.None, out dobProducer))
                        {
                            // Your input string can (and will) be parsed with MM/dd/yyyy format.
                        }
                        else
                        {
                            // Invalid format or value.
                        }
                        im.AddActor(producerName, dobProducer);
                        Console.WriteLine("\n");
                        break;
                    case 5:
                        Console.WriteLine("Program to Delete Movie");
                        var allMovie = im.GetMovies();
                        if (allMovie.Count > 0)
                        {
                            foreach (var movie in allMovie)
                                Console.WriteLine($"{movie.Name}\t");
                            
                            Console.WriteLine("Enter Name of the movie you want to delete");
                            var movieNameDelete = Console.ReadLine();
                            im.DeleteMovie(movieNameDelete);
                        }
                        else
                            Console.WriteLine("No Movie available with this name.");
                        break;
                    case 6:
                        System.Environment.Exit(0);
                        break;

                    default:
                        Console.WriteLine("Wrong Choice Entered ! Please try out again.");
                        break;
                }
            }
        }
    }
}