﻿using IMDB_DEMO;
using System;
using TechTalk.SpecFlow;
using IMDB.Domain;
using System.Collections.Generic;
using TechTalk.SpecFlow.Assist;
using System.Globalization;
using NUnit.Framework;


namespace IMDB_test.Test
{
    [Binding]
    public class SpecFlowFeature1Steps
    {
        private string plot;
        private int y;
        private string movieName;
        List<int> actorId = new List<int>();
        List<int> producerId = new List<int>();
        IMDBServices im = new IMDBServices();
        List<Movie> availableMovie = new List<Movie>();
        [BeforeScenario(new string[] { "listMovie" })]
        public void BeforeSenario()
        {
            im.AddActor("James", DateTime.ParseExact("24/01/2013", "dd/MM/yyyy", CultureInfo.InvariantCulture));
            im.AddActor("Daniel", DateTime.ParseExact("04/01/2011", "dd/MM/yyyy", CultureInfo.InvariantCulture));

            im.AddProducer("Steve", DateTime.ParseExact("20/01/2000", "dd/MM/yyyy", CultureInfo.InvariantCulture));
            im.AddProducer("Marcus", DateTime.ParseExact("10/01/2005", "dd/MM/yyyy", CultureInfo.InvariantCulture));

            im.AddMovie("Deadpool 1", "Deadpool journey", 2019, new List<int> { 1 }, new List<int> { 1 });
            im.AddMovie("Avengers", "Civil War", 2010, new List<int> { 2 }, new List<int> { 2 });
            
        }


        [Given(@"I want to add ""(.*)""")]
        public void GivenIWantToAdd(string p0)
        {
            movieName = p0;
        }

        [Given(@"the year of relese is ""(.*)""")]
        public void GivenTheYearOfReleseIs(int p0)
        {
            y = p0;
        }

        [Given(@"the plot is ""(.*)""")]
        public void GivenThePlotIs(string p0)
        {
            plot = p0;
        }

        [Given(@"the actor id's are ""(.*)""")]
        public void GivenTheActorIdSAre(int p0)
        {
            actorId.Add(p0);
        }

        [Given(@"the producer id's are ""(.*)""")]
        public void GivenTheProducerIdSAre(int p0)
        {
            producerId.Add(p0);
        }

        [When(@"I call the AddMovie function")]
        public void WhenICallTheAddMovieFunction()
        {
            im.AddMovie(movieName,plot,y,actorId,producerId);
        }

        [Given(@"listing movies")]
        public void GivenListingMovies()
        {
            
        }
        
        [When(@"I fetch the movies")]
        public void WhenIFetchTheMovies()
        {
           availableMovie = im.GetMovies();
        }
        
        [Then(@"movies table will look like")]
        public void ThenMoviesTableWillLookLike(Table table)
        {
            //table.CompareToSet(availableMovie);
            availableMovie = im.GetMovies();
            var movieTable = table.CreateSet<(string Name, string Plot, int YOR)>();
            int i = 0;
            foreach (var movie in movieTable)
            {
                Assert.AreEqual(movie.Name, availableMovie[i].Name);
                Assert.AreEqual(movie.Plot, availableMovie[i].Plot);
                Assert.AreEqual(movie.YOR, availableMovie[i].YOR);
                i++;
            }
        }
        
        [Then(@"Actors table will look like")]
        public void ThenActorsTableWillLookLike(Table table)
        {
            var actorTable = table.CreateSet<(string Movie_Name,string Actor_Name)>();
            availableMovie = im.GetMovies();
            foreach(var movie in availableMovie)
            {
                List<String> expectedActor = new List<string>();
                var availableActor = new List<String>();
                foreach (var actor in movie.ActorList)
                    availableActor.Add(actor.Name);
                foreach(var actorRow in actorTable)
                {
                    if (actorRow.Movie_Name == movie.Name)
                        expectedActor.Add(actorRow.Actor_Name);
                }
                Assert.AreEqual(availableActor, expectedActor);
            }
        }
        
        [Then(@"Producers table will look like")]
        public void ThenProducersTableWillLookLike(Table table)
        {
            var producerTable = table.CreateSet<(string Movie_Name, string Producer_Name)>();
            availableMovie = im.GetMovies();
            foreach (var movie in availableMovie)
            {
                List<String> expectedProducer = new List<string>();
                var availableProducer = new List<String>();
                foreach (var producer in movie.ProducerList)
                    availableProducer.Add(producer.Name);
                foreach (var producerRow in producerTable)
                {
                    if (producerRow.Movie_Name == movie.Name)
                        expectedProducer.Add(producerRow.Producer_Name);
                }
                Assert.AreEqual(availableProducer, expectedProducer);
            }
        }
    }
}
