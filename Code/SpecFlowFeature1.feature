﻿Feature: SpecFlowFeature1
	IMDB


@listMovie
Scenario: List Movies
	Given listing movies 
	When I fetch the movies 
	Then movies table will look like 
	| Name       | Plot             | YOR  |
	| Deadpool 1 | Deadpool journey | 2019 |
	| Avengers   | Civil War        | 2010 |
	And Actors table will look like 
	| Movie_Name | Actor_Name |
	| Deadpool 1 | James      |
	| Avengers   | Daniel     | 
	And Producers table will look like 
	| Movie_Name | Producer_Name |
	| Deadpool 1 | Steve         |
	| Avengers   | Marcus        |

@listMovie
Scenario: Add Movie
	Given I want to add "DanceIndia" 
	And the year of relese is "2019"
	And the plot is "India's biggest show"
	And the actor id's are "1"
	And the producer id's are "1"
	When I call the AddMovie function
	Then movies table will look like 
	| Name       | Plot                 | YOR  |
	| Deadpool 1 | Deadpool journey     | 2019 |
	| Avengers   | Civil War            | 2010 |
	| DanceIndia | India's biggest show | 2019 |
	And Actors table will look like 
	| Movie_Name | Actor_Name |
	| Deadpool 1 | James      |
	| Avengers   | Daniel     |
	| DanceIndia | James      |
	And Producers table will look like 
	| Movie_Name | Producer_Name |
	| Deadpool 1 | Steve         |
	| Avengers   | Marcus        |
	| DanceIndia | Steve         |