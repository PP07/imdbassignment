﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMDB.Domain;
namespace IMDB
{
    public class ActorRepository
    {
        private readonly List<Actor> _actors;

        public ActorRepository()
        {
            _actors = new List<Actor>();
        }

        public void Add(Actor actor)
        {

            _actors.Add(actor);
        }

        public List<Actor> Get()
        {
            return _actors.ToList();
        }
    }
}
